<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/dam2-matias/escacs">
    <img src="Resources/icon.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Els escacs a la guerra freda</h3>

<!-- TABLE OF CONTENTS -->

## Taula de continguts

- [Sobre el projecte](#sobre-el-projecte)
  - [Fet Amb](#fet-amb)
- [Instruccions](#instruccions)
  - [Utilització](#utilització)
  - [Pantalla inicial](#pantalla-inicial)
  - [Pantalla de joc](#pantalla-de-joc)
    - [Moviment de peces](#moviment-de-peces)
    - [Captura de peces](#captura-de-peces)
    - [Promoció de peça](#promoció-de-peça)
    - [Panell de moviments](#panell-de-moviments)
- [Començant](#començant)
  - [Requisits previs](#requisits-previs)
  - [Instal·lació](#installació)
- [Contribuir](#contribuir)
- [Llicència](#llicència)

<!-- ABOUT THE PROJECT -->

## Sobre el projecte

Aquest projecte va ser un treball per a l'assignatura de disseny d'interfícies
gràfiques.
El projecte consisteix en un joc d'escacs ambientat en la guerra freda en l'època en la qual els Estats Units i l'URSS s'enforataven en concursos d'escacs.

### Fet Amb

El joc està fet amb VB.NET utilitza classes per a les peces, tauler, partida i les caselles.

- [VB.net](https://es.wikipedia.org/wiki/Visual_Basic_.NET)

<!-- GETTING STARTED -->
## Instruccions
### Utilització

Descarreguem la aplicació i fem doble click en la aplicació.

### Pantalla inicial
___
En aquesta pantalla podem observar dos quadres de text on podem introduir el nostre nom, aixo es opcional.
Tambe podem observar un checkbox per seleccionar quin jugador jugara amb les peces blanques.

<details>
  <img src="imagenes_tuto/inicio.png">
</details>

### Pantalla de joc
___
  #### Moviment de peces
  Les caselles on la peça pot avançar es mostraran de color verd.
  <details>
    <img src="imagenes_tuto/casilla_marcada.png">
  </details>

  #### Captura de peces
  Quan una peça pot captura una peça contraria la casella es marcara de color vermell.
  <details>
    <img src="imagenes_tuto/captura.png">
  </details>

  #### Promoció de peça
  Quan un peó arriba al final del tauler aquest pot promocionar a una altra peça, si fem click en el peó se obrira un nou menu que ens permetra     promocionar la peça, fem click a la peça que volem promocionar (Segona imatge).
  <details>
    <img src="imagenes_tuto/promocion.png">
    <img src="imagenes_tuto/promocion2.png">
  </details>
  #### Panell de moviments

  En la part superior dreta podem observar un panell on es mostren els ultims 10 moviments realizats.
  <details>
    <img src="imagenes_tuto/juego.png">
  </details>

## Començant

Per a obtenir una còpia d'aquest projecte segueix els passos següents:

### Requisits previs

Primer és necessari descarregar Visual Studio Community 2019

Es pot descarregar des de aquest [link](https://visualstudio.microsoft.com/es/vs/community/)

Seguidament és necessari descarregar Git ho podeu descarregar des d'aquest [link](https://git-scm.com/downloads/)

### Instal·lació

1. Clona el repositori

```sh
git clone https://gitlab.com/dam2-matias/escacs.git
```

2. Obre el repositori clonat amb Visual Studio Code

<!-- CONTRIBUTING -->

## Contribuir

Les contribucions fan que la comunitat de codi obert sigui un lloc increïble per aprendre, inspirar i crear. Qualsevol contribució que feu sera **molt apreciada**.

1. Has de fer un fork del projecte
2. Creeu la vostra branca de funcions (`git checkout -b feature/AmazingFeature`)
3. Feu un commit dels vostres canvis (`git commit -m 'Add some AmazingFeature'`)
4. Pujeu els canvis a la branca (`git push origin feature/AmazingFeature`)
5. Obriu una sol·licitud de Pull

<!-- LICENSE -->

## Llicència

Distribuït sota la llicència MIT.
