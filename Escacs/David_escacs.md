# Escacs

Escacs és un joc fet amb Visual Studio que permet jugar partides al joc dels escacs.

## Instal·lació

##### Opcio 1
Obrir la consola de comandos. Escriure la ruta del programa.

Ruta d'exemple
```bash
C:\Users\Person\Desktop\JocEscacs.exe
```

##### Opcio 2
Descarregar el programa i obrir-lo fent doble clic a l'executable.

## Funcionament del programa
A l'obrir el programa ens dona opció de posar nom als dos jugadors.
En cas de que no posem nom serà el sistema el que ens donarà el nom per defecte (Jugador 1 o 2).

Al clicar "Jugar", s'obra el tauler d'escacs i comença el joc.

## Creadors
Aquest programa ha estat creat per David Ribalaigua.

## Llicència
[MIT](https://choosealicense.com/licenses/mit/ "MIT")