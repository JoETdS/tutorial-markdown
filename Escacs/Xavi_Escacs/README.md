# Pràctica 1-1 - URSS la guerra freda i els escacs

Joc d'escacs creat com aplicació d'escriptori de Windows Forms en Visual Basic

---

## Índex

[Funcionalitats del programa](#funcionalitats-del-programa)

[Funcionament del programa](#funcionament-del-programa)

[Interaccions amb el programa](#interaccions-amb-el-programa)

---

### Funcionalitats del programa

- [X] Selecció de les peces
- [X] Moviment de les peces
- [X] Gestió de torns
- [X] Mort de les peces
- [X] Canvi d'un peó a reina al arribar al final
- [X] Detecció d'escac després de moure o matar una peça
- [ ] Detecció d'escac i mat

---

### Fucionament del programa

En executar l'aplicació es mostrarà el tauler amb les peces col·locades i la partida iniciada on el torn inicial és de les peces blanques.

#### Interaccions amb el programa

| Tipus d'interacció|Descripció | Imatge |
| ------|---|----|
| Seleccionar / Desseleccionar una peça | Si es fa clic a una peça del color de les que tenen el torn, es posarà el seu fons de color verd, i les caselles on es pot desplaçar es posaran de color blau. Per desseleccionar-la tornem a clicar la peça|      ![T1](imatges/t1.jpg)    |
| Moure la peça seleccionada | La peça es desplaçarà a la casella clicada sempre que aquesta estigui de color blau. Si hi ha una peça enemiga, aquesta serà matada i portada a la zona de peces mortes    |    ![T2](imatges/t2.jpg)        |
| Reiniciar la partida  |  Es pot reiniciar la partida en qualsevol moment prement el botó "Nova partida" situat a la dreta del tauler |  ![T3](imatges/t3.jpg)  |
| Situació d'escac |  Quan es produeix una situació d'escac apareixerà un missatge indicant-ho, i el rei en situació de risc es posarà amb el fons de color vermell| ![T4](imatges/t4.jpg) ![T5](imatges/t5.jpg)|
| Final de la partida | Quan un jugador mati al rei enemic, apareixerà un missatge felicitant la victòria. En acceptar aquest missatge la partida es reiniciarà | ![T6](imatges/t6.jpg) |
