# Projecte d'Escacs
### Instal·lació del programa
El programa en qüestió no requereix cap tipus instal·lació, sino que simplement donant-li dos clicks sobre el arxiu anomenat Escacs.exe el programa s'iniciarà.

### Funcionament del programa
##### Menú inicial
Un cop obert el executable, ens hauria d'aparèixer una finestra com la mostrada a continuació:
![Menu Escacs](Menu_Escacs.png)

   
En aquest formulari el que ens permetrà es introduir el nom dels 2 usuaris a jugar la partida en els requadres en blanc  , en cas que no s'introdueixi res, s'autoanomenaràn usuari 1 o usuari 2 en cada cas. Un cop elegits els noms, al mig es mostra el botó Iniciar, el qual fa començar la partida i passar al formulari següent.

##### Pantalla principal
A continuació se'ns mostrarà una pantalla com aquesta: 

![Principal Escacs](Principal_Escacs.png)

Aquesta és la pantalla principal on els seus principals elements són: 
- Els noms dels jugadors

- Un botó per a cada jugador : Aquest botó el que realitzarà serà parar el seu contador, passar el torn al jugador contrari i reprendre el contador del mateix.

- Un contador per a cada jugador: Els 2 jugadors tenen un contador de 10 minuts cada un, en cas que a algú dels dos se li acabés el temps, automàticament el jugador contrari haurà guanyat la partida.
Aquí en podem observar un exemple:
![Interacció FI](Interaccio_Final.png)

- El taulell amb les seves respectives fitxes