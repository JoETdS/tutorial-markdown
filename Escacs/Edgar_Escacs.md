## PROYECTO ESCACS JOEL
Este proyecto trata de crear una partida de ajedrez y indicar quien es el ganador de la partida.
## FUNCIONAMIENTO DEL PROGRAMA
El programa tiene distintas funciones para que el juego funcione perfectamente.
Las funciones que tiene el proyecto son las siguientes:
1- **Mover las piezas por todo el tablero-->** Esta función indica que todas las piezas pueden moverse por todo el tablero.
2- **Un contador para poder calcular el tiempo que se tarda en terminar la partida-->** Esta función indica cuanto tiempo dura la partida
3- **Otra funció que he implementado ha sido la función de canvio de jugador-->** Esta función indica cundo se hace cada cambio de jugador es decir cuando el jugador blanco termina de hacer su movimiento, le pasa el turno al jugador negro que hara su movimiento hasta que se termine la partida.
4- **He hecho un intento de una función de eliminación de las piezas-->** Esta función estuvo pensada para que se eliminasen las piexas quando el jugador hubiese querido.
## INTERACCCIONES DEL SISTEMA
Mi proyecto tiene distintas interacciones que lo que hacen junto a las funciones es hacer funcionar la interficie grafica.
Mi interfície tiene las siguientes interacciones:
1- **Saber que jugador empieza-->** El que he hecho es poner un text box con la siguiente preguta: 
*¿Que jugador empieza?* 
Y la respuesta que da la interfice es: 
*Empieza el que tenga la piezas de color blancas* 
De esta manera he indicado que jugador empieza.
2- **Saber a donde mover la pieza-->** El que he hecho es indicar para indicar todas las celdas es crear un array de 64 celdas. Una vez hecho esto cree una funcion llamada *tirar* para indicar a que celda quiero que vaya la pieza.
3- **Saber cuando se hace el cambio de jugador-->**Para indicar el cambio de jugador el que he hecho es crear dos funciones una para indicar el canvio de jugador de blancas a negras y otra función para indicar el canvio de jugador de negras a blancas. 
4- **Poner un temporizador-->** He creado un temporizador para saber cuanto se tarda en finalizar la partida.
5- **Intento de movimiento de eliminación de piezas-->** Finalmente no he podido crear la función de matar.

