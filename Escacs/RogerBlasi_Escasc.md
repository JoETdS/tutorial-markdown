# INSTALACIÓN/Ejecución: 
Para hacer funcionar el progrma, lo único que debes hacer es **ejecutar el .exe** y este deberia lanzar el prgrama y una vez que este **esté cargado**, ya se puede **jugar**.

# USO
Para usar el programa, es muy simple, el funcionamiento de este consta en hacer click encima de las imagenes para moverlas, es decir si quieres mover un peón, lo unico que debes hacer es clicar encima de él y moverlo a la casilla que quieras, así funcionan todas las piezas.
**Hay que tener en cuenta que el programa no está "implementado al 100%" por lo que si clicas en un peón y despues clicas en otro, ninguna de las piezas se moverá ni se "matará".**

# INTERACCIONES CON EL USUARIO
Las unicas interacciones que tiene el porgrama con el usuario son el hecho de poder mover las piezas por el tablero y tambien que cunado el contador que hay en la parte superior derecha llegue a zero se mostrará un mensaje emergente por pantalla que dira que la partida ha terminado.