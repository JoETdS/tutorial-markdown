# Ajedrez con Visual Basic

En este programa veremos una aproximación a un juego de ajedrez con la idea de las batallas que tuvieron la Unión Soviética y los Estados Unidos

## Interfaz

Una vez pulsado el icono de jugar en la primera pantalla aparecerá un tablero y a la derecha una zona a parte en dicha zona se puede ver:

- Un botón para reiniciar la partida, por si el juego se a acabado o simplemente se quiera volver a empezar des de 0
- Una bandera de la potencia que está jugando en ese instante
- Una barra de progreso de las muertes de fichas de un color
- una caja donde se ven las 3 últimas piezas de un color que han sido eliminadas.

## Movimiento
Para el movimiento seguiremos las reglas normales del ajedrez, es decir, no está programado que una pieza pueda moverse solo donde esa pieza pueda, así que se confía que las personas que jueguen sigan las reglas de los movimientos.
