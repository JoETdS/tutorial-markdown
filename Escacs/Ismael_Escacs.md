<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="Escacs_0"></a>Escacs</h1>
<h4 class="code-line" data-line-start=1 data-line-end=2 ><a id="V_Clssica_1"></a>V. Clàssica</h4>
<p class="has-line-data" data-line-start="3" data-line-end="4"><img src="https://camo.githubusercontent.com/b9d13cb436699dd6d52affb8d11c3a5f8ecf2da8058748ff217a5acd98614974/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f73746162696c6974792d6578706572696d656e74616c2d6f72616e67652e7376673f7374796c653d666c61742d737175617265" alt="Stability"></p>
<p class="has-line-data" data-line-start="5" data-line-end="6"><img src="https://i.imgur.com/jHZCc1N.png" alt="VB"></p>
<p class="has-line-data" data-line-start="7" data-line-end="8">Programa d’escacs.</p>
<h2 class="code-line" data-line-start=8 data-line-end=9 ><a id="Continguts_8"></a>Continguts</h2>
<ul>
<li><a href="#Instalacio">Instal·lació</a></li>
<li><a href="#Funcionament">Funcionament</a></li>
<li><a href="#Llicencia">Llicència</a></li>
</ul>
<h2 class="code-line" data-line-start=10 data-line-end=11 ><a name="Instalacio" id="Installaci_10"></a>Instal·lació</h2>
<p class="has-line-data" data-line-start="11" data-line-end="12">Només s’ha d’executar l’arxiu &quot;<strong>Escacs.exe</strong>&quot; dins de la carpeta &quot;<strong>Programa (Executable)</strong>&quot;</p>
<h3 class="code-line" data-line-start=12 data-line-end=13 ><a id="Configuracions_dins_de_laplicaci_12"></a>Configuracions dins de l’aplicació</h3>
<p class="has-line-data" data-line-start="13" data-line-end="14"><img src="https://i.imgur.com/JsE2UDM.png" alt="Vista interfície d'opcions"></p>
<ol>
<li class="has-line-data" data-line-start="14" data-line-end="15">Volum <strong>general</strong> del programa</li>
<li class="has-line-data" data-line-start="15" data-line-end="16">Volum de la <strong>música</strong></li>
<li class="has-line-data" data-line-start="16" data-line-end="17">Volum del <strong>so</strong> (botons)</li>
<li class="has-line-data" data-line-start="17" data-line-end="19">No implementat (Deshabilitat)</li>
</ol>
<h4 class="code-line" data-line-start=19 data-line-end=20 ><a id="Alternativa_per_silenciar_el_so_io_msica_19"></a>Alternativa per silenciar el so i/o música</h4>
<p class="has-line-data" data-line-start="20" data-line-end="21">En la interfície d’<strong>inici</strong> s’hi troben dos botons els quals silencien el so i/o la música:</p>
<p class="has-line-data" data-line-start="22" data-line-end="23"><img src="https://i.imgur.com/pKunArl.png" alt="Botons silenciar inici"></p>
<h2 class="code-line" data-line-start=23 data-line-end=24 ><a name="Funcionament" id="Funcionament_23"></a>Funcionament</h2>
<h3 class="code-line" data-line-start=24 data-line-end=25 ><a id="Inici_24"></a>Inici</h3>
<p class="has-line-data" data-line-start="25" data-line-end="27">Pantalla principal del programa:<br>
<img src="https://i.imgur.com/itEvgfU.png" alt="Vista funcionalitats inici"></p>
<ol>
<li class="has-line-data" data-line-start="27" data-line-end="28">Crea una <strong>nova partida</strong>.</li>
<li class="has-line-data" data-line-start="28" data-line-end="29">Accedeix a la interfície d’<strong>opcions</strong>.</li>
<li class="has-line-data" data-line-start="29" data-line-end="30"><strong>Sortir</strong> de l’aplicació.</li>
<li class="has-line-data" data-line-start="30" data-line-end="31"><strong>Silenciar</strong> el so i/o música</li>
<li class="has-line-data" data-line-start="31" data-line-end="32"><strong>Regles</strong> dels escacs.</li>
</ol>
<h3 class="code-line" data-line-start=32 data-line-end=33 ><a id="Joc_32"></a>Joc</h3>
<p class="has-line-data" data-line-start="33" data-line-end="34"><img src="https://i.imgur.com/dgxr0xe.png" alt="Vista interfície de joc"></p>
<ol>
<li class="has-line-data" data-line-start="34" data-line-end="35">Registra <strong>cada moviment</strong> dels dos jugadors (Num.Moviment, Peça, Posició).</li>
<li class="has-line-data" data-line-start="35" data-line-end="36">Fitxes <strong>menjades</strong> de cada jugador.</li>
<li class="has-line-data" data-line-start="36" data-line-end="37"><strong>Temps</strong> restant.</li>
<li class="has-line-data" data-line-start="37" data-line-end="38"><strong>Nom</strong> de cada jugador.</li>
<li class="has-line-data" data-line-start="38" data-line-end="39"><strong>Torn</strong> actual.</li>
</ol>
<h4 class="code-line" data-line-start=39 data-line-end=40 ><a id="Moviments_de_les_peces_39"></a>Moviments de les peces</h4>
<p class="has-line-data" data-line-start="40" data-line-end="42">Les peces es mouen exactament igual que en els escacs, només deixa realitzar moviments vàlids amb un sistema de clicar a la peça i seleccionar la posició on anirà.<br>
Aspectes a destacar:</p>
<ul>
<li class="has-line-data" data-line-start="42" data-line-end="43">A diferència dels escacs normals, per motius de temps, per guanyar s’ha de menjar el rei contrari.</li>
<li class="has-line-data" data-line-start="43" data-line-end="44">Una vegada guanyada la partida, s’ha de reiniciar tot el programa per realitzar una de nova.</li>
<li class="has-line-data" data-line-start="44" data-line-end="45">El botó de continuar no està implementat.</li>
</ul>
<h2 class="code-line" data-line-start=45 data-line-end=46 ><a name="Llicencia" id="Llicncia_45"></a>Llicència</h2>
<p class="has-line-data" data-line-start="46" data-line-end="47"><a href="https://opensource.org/licenses/MIT">MIT</a></p>