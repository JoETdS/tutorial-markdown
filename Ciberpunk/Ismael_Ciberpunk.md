<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="Gestor_de_BBDD_ONU_0"></a>Gestor de BBDD ONU</h1>
<p class="has-line-data" data-line-start="1" data-line-end="2"><img src="https://camo.githubusercontent.com/b9d13cb436699dd6d52affb8d11c3a5f8ecf2da8058748ff217a5acd98614974/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f73746162696c6974792d6578706572696d656e74616c2d6f72616e67652e7376673f7374796c653d666c61742d737175617265" alt="Stability"></p>
<p class="has-line-data" data-line-start="3" data-line-end="4"><img src="https://i.imgur.com/jHZCc1N.png" alt="VB"></p>
<p class="has-line-data" data-line-start="5" data-line-end="7">Programa gestor de bases de dades per a l’administració de persones.<br>
Permet:</p>
<ul>
<li class="has-line-data" data-line-start="7" data-line-end="8">Afegir persones</li>
<li class="has-line-data" data-line-start="8" data-line-end="9">Cercar persones</li>
<li class="has-line-data" data-line-start="9" data-line-end="10">Editar persones</li>
</ul>
<h2 class="code-line" data-line-start=10 data-line-end=11 ><a id="Continguts_10"></a>Continguts</h2>
<ul>
<li class="continguts"><a href="#Instalacio">Instal·lació</a></li>
<li class="continguts"><a href="#Funcionament">Funcionament</a></li>
<li class="continguts"><a href="#Llicencia">Llicència</a></li>
</ul>
<h2 class="code-line" data-line-start=12 data-line-end=13 ><a name = "Instalacio" id="Installaci_12"></a>Instal·lació</h2>
<h3 class="code-line" data-line-start=13 data-line-end=14 ><a id="Importar_BBDD_13"></a>Importar BBDD</h3>
<p class="has-line-data" data-line-start="14" data-line-end="16">Importació de la base de dades al gestor:<br>
<img src="https://i.imgur.com/M5zbUbW.png" alt="Exemple d'importació de BBDD" title="Importació SQL"></p>
<h3 class="code-line" data-line-start=16 data-line-end=17 ><a id="Connexi_amb_la_base_de_dades_16"></a>Connexió amb la base de dades</h3>
<p class="has-line-data" data-line-start="17" data-line-end="18">Les credencials per defecte de la base de dades són:</p>
<ul>
<li class="has-line-data" data-line-start="18" data-line-end="19">Servidor: <strong>localhost</strong></li>
<li class="has-line-data" data-line-start="19" data-line-end="20">Usuari: <strong>root</strong></li>
<li class="has-line-data" data-line-start="20" data-line-end="21">Contrasenya: <strong>alumne</strong></li>
</ul>
<h4 class="code-line" data-line-start=21 data-line-end=22 ><a id="Canviar_credencials_21"></a>Canviar credencials</h4>
<p class="has-line-data" data-line-start="22" data-line-end="28">Dins de la classe &quot;<strong>BBDDControlador</strong>&quot; es poden modificar les credencials<br>
‘’’<br>
Private serv_bbdd As String = “localhost”<br>
Private user_bbdd As String = “root”<br>
Private pass_bbdd As String = “alumne”<br>
‘’’</p>
<h2 class="code-line" data-line-start=28 data-line-end=29 ><a name = "Funcionament" id="Funcionament_28"></a>Funcionament</h2>
<h3 class="code-line" data-line-start=29 data-line-end=30 ><a id="Iniciar_Sessi_29"></a>Iniciar Sessió</h3>
<p class="has-line-data" data-line-start="30" data-line-end="31">Les credencials per defecte són:</p>
<ul>
<li class="has-line-data" data-line-start="31" data-line-end="32">Usuari: <strong>root</strong></li>
<li class="has-line-data" data-line-start="32" data-line-end="33">Contrasenya: <strong>alumne</strong></li>
</ul>
<h4 class="code-line" data-line-start=33 data-line-end=34 ><a id="Canviar_credencials_33"></a>Canviar credencials</h4>
<p class="has-line-data" data-line-start="34" data-line-end="36">Per crear o canviar les credencials s’ha d’accedir a la taula &quot;<strong>users</strong>&quot; dins de la BBDD:<br>
<img src="https://i.imgur.com/jQIZbzI.png" alt="Vista de la taula users"></p>
<ol>
<li class="has-line-data" data-line-start="36" data-line-end="37">Taula</li>
<li class="has-line-data" data-line-start="37" data-line-end="38">Camps</li>
<li class="has-line-data" data-line-start="38" data-line-end="39">Afegir credencials</li>
</ol>
<h3 class="code-line" data-line-start=39 data-line-end=40 ><a id="Inici_i_men_39"></a>Inici i menú</h3>
<p class="has-line-data" data-line-start="40" data-line-end="41"><img src="https://i.imgur.com/la8Cke5.png" alt="Vista inici i funcionalitats del menú"></p>
<ol>
<li class="has-line-data" data-line-start="41" data-line-end="42">Botó per anar a <strong>inici</strong>.</li>
<li class="has-line-data" data-line-start="42" data-line-end="43">Botó per anar a <strong>gestió de persones</strong>.</li>
<li class="has-line-data" data-line-start="43" data-line-end="44">Botó per <strong>sortir</strong> de l’aplicació.</li>
</ol>
<h3 class="code-line" data-line-start=44 data-line-end=45 ><a id="Afegir_persones_44"></a>Afegir persones</h3>
<p class="has-line-data" data-line-start="45" data-line-end="47"><img src="https://i.imgur.com/8N5TMiv.png" alt="Vista afegir persones"><br>
Coses a tenir en compte:</p>
<ul>
<li class="has-line-data" data-line-start="47" data-line-end="48">S’han d’omplir <strong>TOTS</strong> els camps per poder afegir una persona (menys la fotografia)</li>
<li class="has-line-data" data-line-start="48" data-line-end="49">En el cas que no s’omplin tots els camps o s’afegeixi una persona, la interfície es reiniciarà, és a dir, es netejarà tota la informació dels camps.</li>
</ul>
<h3 class="code-line" data-line-start=49 data-line-end=50 ><a id="Cercar_persones_49"></a>Cercar persones</h3>
<p class="has-line-data" data-line-start="50" data-line-end="52"><img src="https://i.imgur.com/dIJO9eZ.png" alt="Vista cerca de persones"><br>
Aspectes a destacar:</p>
<ul>
<li class="has-line-data" data-line-start="52" data-line-end="53">S’ha d’afegir <strong>1 o més</strong> camps per realitzar la cerca.</li>
<li class="has-line-data" data-line-start="53" data-line-end="54">En cas que <strong>no</strong> es trobi cap persona amb les dades introduïdes, tots els camps es reiniciaran.</li>
<li class="has-line-data" data-line-start="54" data-line-end="55">En el cas que <strong>si</strong> es trobi una o més persones, s’obrirà una nova interfície amb els resultats trobats.</li>
</ul>
<h3 class="code-line" data-line-start=55 data-line-end=56 ><a id="Resultats_55"></a>Resultats</h3>
<p class="has-line-data" data-line-start="56" data-line-end="58"><img src="https://i.imgur.com/wxi6U4x.png" alt="Vista resultats"><br>
En el cas que es trobin persones realitzant una cerca podem:</p>
<ul>
<li class="has-line-data" data-line-start="58" data-line-end="59">Modificar les dades de la les persones</li>
<li class="has-line-data" data-line-start="59" data-line-end="60">Una vegada aplicats els canvis es pot tornar a cercar persones, la interfície es reinicia automàticament amb cada cerca.</li>
</ul>
<h2 class="code-line" data-line-start=60 data-line-end=61 ><a name = "Llicencia" id="Llicncia_60"></a>Llicència</h2>
<p class="has-line-data" data-line-start="61" data-line-end="62"><a href="https://opensource.org/licenses/MIT">MIT</a></p>