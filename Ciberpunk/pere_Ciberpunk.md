# Aplicació de Control i Estabilitat Social
![CyberEye](https://i.pinimg.com/564x/61/ec/db/61ecdb657b543051771ef5dded05e294.jpg)
## INSTALACIÓ

Per a instal·lar ACES, 
 1. Descarrega primer l'arxiu comprimit del repositori
 2. Descomprimeix-lo
 3. Executa el .exe

Es recomana tambè descarregar la base de dades per a importar-la en el vostre gestor. Aquesta té persones inventades que podrem utilitzar per a proves.

Si no saps com importar una base de dades, segueix el que et convingui dels següents tutorials: 
- [phpMyAdmin](https://help.one.com/hc/es/articles/115005588189--C%C3%B3mo-importar-una-base-de-datos-a-phpMyAdmin-)
- [MariaDB](https://www.stackscale.com/es/blog/importar-exportar-bases-datos-mysql-mariadb/)
- [SQLite](https://www.dokry.com/19733)
- [PostgreSQL](https://viviryaprenderweb.com/como-importar-una-base-de-datos-en-postgresql/)
## FUNCIONAMENT

ACES és una molt potent i sofisticada aplicació per a tenir control sobre nacions o el planeta. Està pensada per un fútur en el que la vida sigui molt complicada, i en el que es necessiti monitoritzar les persones.

> Has de estar conectat a la BD per a poder utilitzar les funcions

Desde el menú principal, podrem accedir a afegir i veure persones. En quan afegim una persona amb tots els paràmetres que desitgem, aquesta es podrà trovar al apartat de veure persona. Un cop allí podrem visualitzar tota la informació introduïda i modificar-la al nostre gust. Tambè tindrem un botó per a suprimir la persona seleccionada de la base de dades.
