# CYBERPUNK
## Pàgina principal 
A la pàgina principal del programa, hi trobarem tres botons, el de Gremi, Persona i Regió, cada un té una funcionalitat diferent.
El de Persona, el que fa és portar-te al formulari de persona; el de Gremi et porta al formulari de gremi i el de Regió, el que fa és portar-te al formulari de regió.

## Pàgina de Persona
A la pàgina de persona, hi podem trobar tres botons diferents, el de "Buscar DNI", el d'"Afegir" i el d'"INICI", cada un amb una funcionalitat diferent
 - Buscar DNI: Aquest botó ens serveix per tal de poder buscar un usuari en concret de dins de la base de dades; per tal de poder buscar a algún usuaari, hem d'introduïr el seu DNI a la barra en blanc que té al costat el botó de buscar DNI; i tota la seva informació sortirà a sota. 
 - Afegir: Aquest botó serveix per poder introduïr un nou usuari a la base de dades; per tal de poder afegir un nou usuari, hem d'omplir els camps de DNI, nom, cognom, idregió i idgremi; i un com hem omplert tots els camps, hem de fer click sobre el botó afegir, i si es crea correctament, ens sortirà un missatge que ens avisarà. 
 - INICI: Aquest botó ens serveix per tal de poder tornar a la pantalla principal del programa. 
 
## Pàgina de Gremi 
A la pàgina de gremi, hi podem trobar tres botons diferents; el botó "Buscar id", el botó "Afegir", i el botó "INICI"; a part podem trobar una llista (CERCA ID) a la part superior esquerre de la pantalla on ens diu la relació que hi ha entre les id i els gremis diferents. 
 - Buscar id: Aquest botó ens serveis per tal de trobar l'informació d'un gremi en concret, per tal de poder-lo fer servir, hem d'omplir el requadre blanc del costat del botó amb la id del gremi que dessitgem buscar; i un cop omplert el requadre, si fem click sobre el botó de 'Buscar id', ens mostrarà la informació del gremi a sota. 
 - Afegir: Aquest botó serveix per poder introduïr un nou gremi a la base de dades; per tal de poder afegir un nou gremi, hem d'omplir els camps de idgemi, nom, i idregió; un com hem omplert tots els camps, hem de fer click sobre el botó afegir, i si es crea correctament, ens sortirà un missatge que ens avisarà. 
 - INICI: Aquest botó ens serveix per tal de poder tornar a la pantalla principal del programa. 
 
## Pàgina de Regió
 A la pàgina de regió, hi podem trobar tres botons diferents; el botó "Buscar id", el botó "Afegir", i el botó "INICI"; a part podem trobar una llista (CERCA ID) a la part superior esquerre de la pantalla on ens diu la relació que hi ha entre les id i les regions diferents. 
 - Buscar id: Aquest botó ens serveis per tal de trobar l'informació d'una regió en concret, per tal de poder-lo fer servir, hem d'omplir el requadre blanc del costat del botó amb la id de la regió que dessitgem buscar; i un cop omplert el requadre, si fem click sobre el botó de 'Buscar id', ens mostrarà la informació de la regió a sota. 
 - Afegir: Aquest botó serveix per poder introduïr una nova regió a la base de dades; per tal de poder afegir una nova regió, hem d'omplir els camps de idregio, nom, i idgremi; un com hem omplert tots els camps, hem de fer click sobre el botó afegir, i si es crea correctament, ens sortirà un missatge que ens abisarà. 
 - INICI: Aquest botó ens serveix per tal de poder tornar a la pantalla principal del programa. 
