# INSTALACIÓN/EJECUCIÓN
Para hacer funcionar el programa, lo que debes hacer es **iniciar** el **servidor MySQL Database** y el **servidor Apache Web Server**, después de eso, asegurarte de que tengas la **base de datos** que usa el **programa** (si no la tienes la puedes conseguir en el enlaze de gitlab donde esta el pryecto) y una vez que te hayas asegurado de que todo lo mencionado anteriormente funcione debes **ejecutar** el **.exe** proporcionado.

# USO
En cuanto al uso del prgrama, este es muy simple. Para empezar, para moverte por el porgrama solo tienes botones los cuales tienen el nombre de lo que hacen.
La primera pantalla, solo tiene un botón que es el de login el cual te mandará a un menú que tiene 3 botones más uno de ellos llamado Sign Out es para volver a la pantalla de login, otro, que se llama ContentPreview te mandará a la pantalla donde puedes visualizar los datos que hay en la BBDD y el otro llamado Operations, te mandará a la pantalla donde puedes hacer inserts o deletes en la BBDD.
En la pantalla de Operaciones, hay 6 botones: conectarse y desconectarse de la base de datos que hacen lo que el propio nombre indica (conectarse o desconectarse de la BBDD), también hay un boton para volver al menú donde hay las 3 opciones y 2 botones más que uno sirve para insertar datos en la BBDD y el otro para borrar **(el boton de borrar solo nocesita saber el id de la persona que deseas borrar)** y el botón para volver a la pantalla de login.
La pantalla de ContentPreview solo tiene un botón a parte de los de volver al menú o a la pantalla de login el cual lo que hace es conectarse a la BBDD y mostrar el contenido en forma de tabla con sus respectivos atributos.

# INTERACCIONES CON EL USUARIO
Las únicas interacciones que hay con el usuario a parte de los botones es que al iniciar un programa, saldrá un mensage y que si alguna de las funcionalidades de los botones falla tambíen saldrá un mensaje por pantalla.