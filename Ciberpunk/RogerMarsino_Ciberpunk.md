# Projecte Ciberpunk
### Instal·lació del programa
El programa en qüestió no requereix cap tipus instal·lació, sino que simplement donant-li dos clicks sobre el arxiu anomenat Ciberpunk.exe el programa s'iniciarà.

### Funcionament del programa
##### Menú inicial
Un cop obert el executable, ens hauria d'aparèixer una finestra com la mostrada a continuació:
![Menu Inici](Menu_Inici.png)

Aquest és el menú d'inici, desde el qual podem realitzar les diferents operacions sobre la base de dades.
Les opcions són les següents:
- Afegir una nova persona a la BBDD.
- Mostrar les dades de una persona a partir de la seva ID
- Eliminar una persona de la BBDD a partir de la seva ID
- Modificar les dades d'una persona a partir de la seva ID


##### Afegir una persona
Al apretar al botó anomenat "Afegir una persona", ens mostrarà el següent formulari:
![Afegir Una Persona](AfegirUnaPersona.png)

En aquest formulari el que ens mostrà uns espais en blanc per a introduir-hi les dades de la persona a afegir a la base de dades. Com podem observar, ens mostra uns asteriscs al costat d'alguns camps. això significarà que són obligatòris d'introduir.
Després ens trobem amb dos botons, un per a tornar al menú inicial i un altre per a a enviar les dades de la persona i crear una nova persona.

Un cop hem completat els camps obligatòris i pulsem a enviar, ens apareixerà si ens conectat o no a la BBDD, i en cas afirmatiu ens sortirà el missatge de que s'ha introduit la persona.

![conectat](conectat.png)
![Usuari Introduit](Usuari_Introduit.png)
*Aquí podem observar les 2 interaccions*

##### Mostrar les dades d'un usuari
Al introduir-li una ID al camp de text i prémer el botó de modificar, primerament ens mostrarà, igual que al anterior, si ens hem conectat o no a la BBDD, enc cas afirmatiu ens mostrarà el següent formulari:

![Dades usuari](DadesUsuari.png)

En aquest formulari simplement ens mostrà les dades del usuari amb la ID introduida, dades que no es poden modificar i l'unica interacció possible és la de tornar al menú inicial amb el botó corresponent.

##### Eliminar un usuari de la BBDD
Al igual que en l'anterior, aquesta interacció funciona introduint-hi la ID corresponent a l'usuari que, en aquest cas, vulguem eliminar. 
Primerament, ens apareixerà si ens hem conectat o no a la Base de Dades, i seguidament ens mostrarà un altre MessageBox per a dir-nos si se'ns ha eliminat l'usuari corresponent.
Aquí ho podem observar:

![Usuari Eliminat](usuariEliminat.png)

##### Modificar les dades d'una persona
A un cop introduit la ID, podem prémer el botó de modificar una persona per a que ens aparegui el següent formulari:

![Modificar Dades](ModificarDades.png)

Aquí podem observar les dades de l'usuari, tot i que a diferencia de la interacció de mostrar dades, aquí es pot modificar qualsevol de les dades mostrades. Un cop modificades les mateixes, podem prémer el botó enviar, amb el qual ens apareixerà el Message Box de si s'ha conectat o no a la Base de Dades, i si s'han modificat o no les dades de l'usuari. Des d'aquest formulari també podem retornar al Menú d'inici en qualsevol moment a partir del botó corresponent.








