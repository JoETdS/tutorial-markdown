# Ciberpunk

Ciberpunk és un programa fet amb Visual Studio que permet consultar, modificar, afegir i esborrar persones de una base de dades.

## Instal·lació

##### Opcio 1
Obrir la consola de comandos. Escriure la ruta del programa.

Ruta d'exemple
```bash
C:\Users\Person\Desktop\Ciberpunk.exe
```

##### Opcio 2
Descarregar el programa i obrir-lo fent doble clic a l'executable.

## Funcionament del programa
A l'obrir el programa ens dona varies opcions a realitzar.

##### Funcionalitats
Al fer clic a afegir podem afegir una persona a la base de dades. Per fer-ho cal emplenar tots els camps pertinents.
Al fer clic a consultar persona podem veure les dades d'una persona. Per fer-ho cal cercar la persona que vulguem.
Al fer clic a modificar o esborrar persona podem veure les dades d'una persona per a modificar-les o bé esborrar aquesta persona. Per fer-ho cal cercar la persona que ens interesi i ens mostrarà totes les seves dades. Si desitjem modificar-les simplement modifiquem els camps que ens interesi. Si desitjem esborrar-la fem clic directament a esborrar.

## Creadors
Aquest programa ha estat creat per David Ribalaigua.

## Llicència
[MIT](https://choosealicense.com/licenses/mit/ "MIT")