## PROYECTO CIBERPUNK

Este proyecto trata de una base de datos en la cual se comprueban las tablas de Personas, Sanidad y Alimentación.

## FUNCIONAMIENTO DEL PROGRAMA
Este proyecto tiene distintas funciones para que el programa funcione correctamente.
La funciones que tiene són las siguientes:
1-**Que el usuario puede introducir datos a las tres bases de datos-->** Para que esta función funcione perfectamente el que hecho es introducir un insert **INTO** dentro de la función *afegirPersona* con lo cual al poner en marcha la interficie grafica se connectara a la base de datos y agregara la persona dentro de su base de datos.
2-**Que el usuario puede elliminar datos de las tres bases de datos-->** Para que esta función funcione perfectamente el que he hecho es introducir un Delete From *el nombre de la tabla* **WHERE** y el nombre de la variable que queremos eliminar una vez que os connecteis a la base la eliminación del dato ser hara automaticamente.
3-**El usuario puEde ver el contenido de las tres tablas-->** Una vez que habeis hecho la inserció y la eliminación de los datos de las bases de datos se os mostrará directo a la interfície todas las bases de datos (cada una refiriendose a su tematica).
A demás contiene distintas interacciones que tiene que hacer el usuario. Las interacciones que hace el usuario són las siguientes:
## REACCIONES DEL SISTEMA
1-**Introducir datos a las bases de datos-->** El usuario puede introducir datos aleatorios refiriendose a la tabla que esta usando.
2-**Eliminar datos de las bases de datos-->** El usuario puede eliminar datos de las tablas con una simple *frase*.
3-**Mostrar contenido de las bases de datos-->** Una vez hecha la introducción y la eliminacaón de los datos de las tablas el usuario puede ver el contenido de las tablas que ha estado utilizando.
Para poderse connectar a las bases de datos el que tienes que hacer es clicar a los botes de *Tabla Persona*, *Tabla Sanidad* y *Tabla Alimentación*. Una vez esteis connectados la inserción de cada tabla se hara y podreis observar el conetenido de las tablas. De la tabla alimentación no he conseguido poder mostrar la tabla por un error.
