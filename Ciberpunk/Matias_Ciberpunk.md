<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/dam2-matias/ciberpunk">
    <img src="Resources/ciberpunk.jpg" alt="Logo" width="786" height="436">
  </a>

# Ciberpunk

<!-- TABLE OF CONTENTS -->

## Tabla de contenidos

- [Sobre el proyecto](#sobre-el-proyecto)
  - [Hecho con](#hecho-con)
- [Utilización](#utilización)
  - [Buscar](#buscar)
  - [Insertar](#insertar)
  - [Eliminar](#eliminar)
  - [Modificar](#modificar)
  - [Mostrar Todo](#mostrar-todo)
- [Pasos](#pasos)
  - [Requisitos previos](#requisitos-previos)
  - [Instalación](#installación)
  - [Configuración](#configuración)
- [Contribuir](#contribuir)
- [Licencia](#licencia)

<!-- ABOUT THE PROJECT -->

## Sobre el proyecto

Este proyecto fue un trabajo para la asignatura de diseño de interfaces
gráficas.
El proyecto consiste en una aplicacion que nos permita consultar información sobre las personas.

### Hecho con

La aplicacion esta hecha con VB.NET utiliza clases para cada tabla de la BD.

- [VB.net](https://es.wikipedia.org/wiki/Visual_Basic_.NET)

<!-- GETTING STARTED -->
## Utilización

Hacemos doble click en la aplicacion y esta se abrira.

### Buscar

En este menu de la aplicación necesitamos introducir el ID de algun usuario y hacer click en el botón "Buscar" (En caso de que el ID sea valido se mostrara otra pantalla incluyendo toda la información de la persona). En caso de no conocer un ID para mas información consultar [Mostrar Todo](#mostrar-todo)

Una vez estamos en esta pantalla podemos modificar y eliminar los datos de dicha Persona. Para mas información consultar [Eliminar](#eliminar) o [Modificar](#modificar).


<details> <img src="imagenes_tuto/buscar.png" alt="Buscar" width="548" height="331"> </details>

### Eliminar
Una vez hemos buscado una persona por el ID se nos mostrara la pantalla de resultado, en esta pantalla podemos observar 2 botones en la parte inferior, Eliminar o Modificar.
Si hacemos click en "Eliminar" se mostrara una ventana emergente que nos preguntara si deseamos eliminar los datos.

<details> <img src="imagenes_tuto/eliminar.png" alt="Buscar" width="548" height="331"> </details>


### Modificar
En esta pantalla tenemos varios campos los cuales podemos rellenar con la información que deseamos, seguidamente si hacemos click en el botón "Modificar" los datos de dicha persona se modificaran.

<details> <img src="imagenes_tuto/modificar.png" alt="Buscar" width="548" height="331"></details>


### Insertar
En esta pantalla tenemos varios campos los cuales podemos rellenar con la información que deseamos, seguidamente si hacemos click en el botón "Insertar" los datos de dicha persona se insertaran en la base de datos.

<details> <img src="imagenes_tuto/insertar.png" alt="Buscar" width="548" height="331"></details>


### Mostrar Todo
Si hacemos click en el botón "Mostrar Todo" del menu principal se mostraran todas las filas de la tabla persona, con toda su información.
Podemos utilizar este apartado si no conocemos el ID de ninguna persona.

<details> <img src="imagenes_tuto/mostrar.png" alt="Buscar" width="548" height="331"></details>


## Pasos

Para clonar el repositorio sigue los pasos siguienties:

### Requisitos previos

Primero es necesario descargar Visual Studio Community 2019

Se puede descargar desde este [link](https://visualstudio.microsoft.com/es/vs/community/)

Seguidamente es necesario descargar Git lo puede descargar desde este [link](https://git-scm.com/downloads/)

### Instalación

1. Clona el repositorio

```sh
git clone https://gitlab.com/dam2-matias/ciberpunk.git
```

2. Abre el repositorio clonado con Visual Studio Code

3. Importar el fichero <a href="https://gitlab.com/dam2-matias/ciberpunk/ciberpunk.sql">ciberpunk.sql</a> en su base de datos

### Configuración

1. Editar la clase SQLHandler y cambiar el usuario y contraseña para conectarse.

## Contribuir

Las contribuciones hacen que la comunidad de código abierto sea un lugar increíble para aprender, inspirar y crear. Cualquier contribución que haga sera **muy apreciada**.

1. Tienes que hacer un fork del proyecto
2. Cree su rama de funciones (`git checkout -b feature /AmazingFeature`)
3. Haga un commit de sus cambios (`git commit -m 'Add some AmazingFeature'`)
4. Suba los cambios a la rama (`git push origin feature /AmazingFeature`)
5. Abra una solicitud de Pull

<!-- LICENSE -->

## Licencia

Bajo la licencia MIT.
