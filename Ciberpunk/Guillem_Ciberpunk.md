# Wintermute

Wintermute es una aplicación de la **UNATCO**, una delegación de la ONU en contra del terrorismo, están haciendo pruebas para ponerla en funcionamiento, y cuando esté lista tendrán el poder de controlar a todos los ciudadanos y erradicar cualquier aversión a la ONU.

Para hacerlo han decidido integrar estos datos en la base de datos:

- DNI
- Nombre
- Apellido 1
- Apellido 2
- Edad
- Región
- Empresa
- Trabajo
- Dirección de vivienda
- Hijos dados a la nación
- Nivel de beneficio en el trabajo
- Nivel de ración de comida
- Peligro de revelación

## Entrando en el programa
Cuando iniciemos él .exe nos presentara una pantalla de inicio de win95, el porqué de esto y todo el diseño viene a que casi todas las obras ciberpunks son antes de los 2000, así que mucha de la estítica se basaba en la imaginación partiendo de su época.

A continuación, entraremos en la sesión de un ordenador y aparecerá un asistente que nos guiará por la app de Wintermute. Debido a un fallo hay que seguir estrictamente lo que dice el asistente y acabar todos los textos hasta que el asistente desaparezca.  Todos los botones que se ven tienen interacción (borrar ultimo insert).

## Últimos avisos
Al pulsar el botón de start en la primera pantalla que vemos se hará la conexión a la base de datos, el estado de la base se podrá ver en la esquina inferior derecha, BD ON o BD OFF.
Por ciertos problemas solo es posible ingresar 1 persona a la base de datos.
