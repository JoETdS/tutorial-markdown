# Pràctica 2 - Cyberpunk

Programa de gestió de ciutadans en un futur distòpic

---

## Índex

[Requisits de funcionament](#requisits-de-funcionament)

[Funcionalitats del programa](#funcionalitats-del-programa)

[Funcionament del programa](#funcionament-del-programa)

---

## Requisits de funcionament

Per tal que el programa funcioni correctament, cal establir "alumne" com contrasenya de l'usuari root de mysql. També cal importar a una base de dades buida amb nom "cyberpunk" el fitxer .sql que acompanya a l'executable.

---

## Funcionalitats del programa

- [X] Creació de persones
- [X] Edició de persones
- [X] Cerca de persones
- [X] Eliminació de persones
- [X] Consulta de dades de persones

---

## Funcionament del programa
En executar l'aplicació, es mostrarà la finestra principal amb les opcions que disposem per gestionar les persones. A la següent taula s'exposa el funcionament del programa en detall.

| Element | Descripció | Imatge |
| ----- | ---------- | ------ |
| Formulari inicial | Pàgina principal del programa, tenim les opcions "Cercar persona", "Crear persona" i "Sortir (Tancarem l'aplicació)" | ![T1](imatgesREADME/T1.jpg) |
| Crear persona | Formulari on podem crear persones noves. Després d'introduïr totes les dades, hem de clicar el botó "Desar" i un missatge ens confirmarà que la persona ha estat creada correctament. No podem deixar cap camp buit. Si cliquem a sortir tornarem al formulari inicial | ![T2](imatgesREADME/T2.jpg) |
| Cercar Persona | Podrem cercar la persona per nom i/o cognom mitjançant el botó "Cercar". Si no introduïm cap nom ni cognom, es mostraran totes les persones de l'aplicació. Seleccionarem el registre a consultar a la llista i clicarem el botó "Obrir" per accedir a les dades de la persona (Consultar persona) | ![T3](imatgesREADME/T3.jpg) |
| Consultar persona | Visualitzem totes les dades de la persona. Per modificar les dades cal clicar al botó "EDITAR". Quan estem editant, ens apareix el botó eliminar, si el cliquem, ens apareixerà un missatge de confirmació d'eliminació de la persona. Un cop hem finalitzat de modificar les dades, cliquem el botó "DESAR" i els camps tornaran a quedar bloquejats | ![T4](imatgesREADME/T4.jpg) ![T5](imatgesREADME/T5.jpg) |